package com.wags.flightreservation.javabeans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author Wilmar Giraldo
 */
@Named(value = "menu")
//@SessionScoped
public class Menu implements Serializable {

    private final int OPT1 = 1;
    private final int OPT2 = 2;
    private final int OPT3 = 3;
    private int selectedOption;

    /**
     * Creates a new instance of Menu
     */

    public Menu() {
        selectedOption = OPT1;
    }

    public int getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(int selectedOption) {
        this.selectedOption = selectedOption;
    }

    public int getOPT1() {
        return OPT1;
    }

    public int getOPT2() {
        return OPT2;
    }

    public int getOPT3() {
        return OPT3;
    }
    
    
}
