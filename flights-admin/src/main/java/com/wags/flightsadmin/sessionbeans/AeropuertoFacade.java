/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.flightsadmin.sessionbeans;

import com.wags.flightsadmin.entities.Aeropuerto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Wilmar Giraldo
 */
@Stateless
public class AeropuertoFacade extends AbstractFacade<Aeropuerto> {

    @PersistenceContext(unitName = "com.wags_flights-admin_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AeropuertoFacade() {
        super(Aeropuerto.class);
    }
    
}
