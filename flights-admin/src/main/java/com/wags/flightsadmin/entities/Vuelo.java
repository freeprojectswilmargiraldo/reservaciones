/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.flightsadmin.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Wilmar Giraldo
 */
@Entity
@Table(name = "vuelo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vuelo.findAll", query = "SELECT v FROM Vuelo v"),
    @NamedQuery(name = "Vuelo.findById", query = "SELECT v FROM Vuelo v WHERE v.id = :id"),
    @NamedQuery(name = "Vuelo.findByOrigenId", query = "SELECT v FROM Vuelo v WHERE v.origenId = :origenId"),
    @NamedQuery(name = "Vuelo.findByDestinoId", query = "SELECT v FROM Vuelo v WHERE v.destinoId = :destinoId"),
    @NamedQuery(name = "Vuelo.findByFecha", query = "SELECT v FROM Vuelo v WHERE v.fecha = :fecha"),
    @NamedQuery(name = "Vuelo.findByHora", query = "SELECT v FROM Vuelo v WHERE v.hora = :hora"),
    @NamedQuery(name = "Vuelo.findByNuevahora", query = "SELECT v FROM Vuelo v WHERE v.nuevahora = :nuevahora"),
    @NamedQuery(name = "Vuelo.findByEstado", query = "SELECT v FROM Vuelo v WHERE v.estado = :estado"),
    @NamedQuery(name = "Vuelo.findByCapacidad", query = "SELECT v FROM Vuelo v WHERE v.capacidad = :capacidad"),
    @NamedQuery(name = "Vuelo.findByDisponibles", query = "SELECT v FROM Vuelo v WHERE v.disponibles = :disponibles"),
    @NamedQuery(name = "Vuelo.findByValorticket", query = "SELECT v FROM Vuelo v WHERE v.valorticket = :valorticket")})
public class Vuelo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "origen_id")
    private int origenId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "destino_id")
    private int destinoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora")
    @Temporal(TemporalType.TIME)
    private Date hora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nuevahora")
    @Temporal(TemporalType.TIME)
    private Date nuevahora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "capacidad")
    private int capacidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "disponibles")
    private int disponibles;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valorticket")
    private long valorticket;
    @OneToMany(mappedBy = "vueloId")
    private Collection<Venta> ventaCollection;
    @JoinColumn(name = "aerolinea_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Aerolinea aerolineaId;
    @JoinColumn(name = "aeropuertodestino_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Aeropuerto aeropuertodestinoId;
    @JoinColumn(name = "aeropuertoorigen_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Aeropuerto aeropuertoorigenId;

    public Vuelo() {
    }

    public Vuelo(Integer id) {
        this.id = id;
    }

    public Vuelo(Integer id, int origenId, int destinoId, Date fecha, Date hora, Date nuevahora, String estado, int capacidad, int disponibles, long valorticket) {
        this.id = id;
        this.origenId = origenId;
        this.destinoId = destinoId;
        this.fecha = fecha;
        this.hora = hora;
        this.nuevahora = nuevahora;
        this.estado = estado;
        this.capacidad = capacidad;
        this.disponibles = disponibles;
        this.valorticket = valorticket;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getOrigenId() {
        return origenId;
    }

    public void setOrigenId(int origenId) {
        this.origenId = origenId;
    }

    public int getDestinoId() {
        return destinoId;
    }

    public void setDestinoId(int destinoId) {
        this.destinoId = destinoId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public Date getNuevahora() {
        return nuevahora;
    }

    public void setNuevahora(Date nuevahora) {
        this.nuevahora = nuevahora;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public int getDisponibles() {
        return disponibles;
    }

    public void setDisponibles(int disponibles) {
        this.disponibles = disponibles;
    }

    public long getValorticket() {
        return valorticket;
    }

    public void setValorticket(long valorticket) {
        this.valorticket = valorticket;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    public Aerolinea getAerolineaId() {
        return aerolineaId;
    }

    public void setAerolineaId(Aerolinea aerolineaId) {
        this.aerolineaId = aerolineaId;
    }

    public Aeropuerto getAeropuertodestinoId() {
        return aeropuertodestinoId;
    }

    public void setAeropuertodestinoId(Aeropuerto aeropuertodestinoId) {
        this.aeropuertodestinoId = aeropuertodestinoId;
    }

    public Aeropuerto getAeropuertoorigenId() {
        return aeropuertoorigenId;
    }

    public void setAeropuertoorigenId(Aeropuerto aeropuertoorigenId) {
        this.aeropuertoorigenId = aeropuertoorigenId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vuelo)) {
            return false;
        }
        Vuelo other = (Vuelo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wags.flightsadmin.entities.Vuelo[ id=" + id + " ]";
    }
    
}
