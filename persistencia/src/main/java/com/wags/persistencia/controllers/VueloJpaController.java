/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.persistencia.controllers;

import com.wags.persistencia.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.wags.persistencia.entities.Aerolinea;
import com.wags.persistencia.entities.Aeropuerto;
import com.wags.persistencia.entities.Venta;
import com.wags.persistencia.entities.Vuelo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wilmar Giraldo
 */
public class VueloJpaController implements Serializable {

    public VueloJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vuelo vuelo) {
        if (vuelo.getVentaCollection() == null) {
            vuelo.setVentaCollection(new ArrayList<Venta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Aerolinea aerolineaId = vuelo.getAerolineaId();
            if (aerolineaId != null) {
                aerolineaId = em.getReference(aerolineaId.getClass(), aerolineaId.getId());
                vuelo.setAerolineaId(aerolineaId);
            }
            Aeropuerto aeropuertodestinoId = vuelo.getAeropuertodestinoId();
            if (aeropuertodestinoId != null) {
                aeropuertodestinoId = em.getReference(aeropuertodestinoId.getClass(), aeropuertodestinoId.getId());
                vuelo.setAeropuertodestinoId(aeropuertodestinoId);
            }
            Aeropuerto aeropuertoorigenId = vuelo.getAeropuertoorigenId();
            if (aeropuertoorigenId != null) {
                aeropuertoorigenId = em.getReference(aeropuertoorigenId.getClass(), aeropuertoorigenId.getId());
                vuelo.setAeropuertoorigenId(aeropuertoorigenId);
            }
            Collection<Venta> attachedVentaCollection = new ArrayList<Venta>();
            for (Venta ventaCollectionVentaToAttach : vuelo.getVentaCollection()) {
                ventaCollectionVentaToAttach = em.getReference(ventaCollectionVentaToAttach.getClass(), ventaCollectionVentaToAttach.getId());
                attachedVentaCollection.add(ventaCollectionVentaToAttach);
            }
            vuelo.setVentaCollection(attachedVentaCollection);
            em.persist(vuelo);
            if (aerolineaId != null) {
                aerolineaId.getVueloCollection().add(vuelo);
                aerolineaId = em.merge(aerolineaId);
            }
            if (aeropuertodestinoId != null) {
                aeropuertodestinoId.getVueloCollection().add(vuelo);
                aeropuertodestinoId = em.merge(aeropuertodestinoId);
            }
            if (aeropuertoorigenId != null) {
                aeropuertoorigenId.getVueloCollection().add(vuelo);
                aeropuertoorigenId = em.merge(aeropuertoorigenId);
            }
            for (Venta ventaCollectionVenta : vuelo.getVentaCollection()) {
                Vuelo oldVueloIdOfVentaCollectionVenta = ventaCollectionVenta.getVueloId();
                ventaCollectionVenta.setVueloId(vuelo);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
                if (oldVueloIdOfVentaCollectionVenta != null) {
                    oldVueloIdOfVentaCollectionVenta.getVentaCollection().remove(ventaCollectionVenta);
                    oldVueloIdOfVentaCollectionVenta = em.merge(oldVueloIdOfVentaCollectionVenta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vuelo vuelo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vuelo persistentVuelo = em.find(Vuelo.class, vuelo.getId());
            Aerolinea aerolineaIdOld = persistentVuelo.getAerolineaId();
            Aerolinea aerolineaIdNew = vuelo.getAerolineaId();
            Aeropuerto aeropuertodestinoIdOld = persistentVuelo.getAeropuertodestinoId();
            Aeropuerto aeropuertodestinoIdNew = vuelo.getAeropuertodestinoId();
            Aeropuerto aeropuertoorigenIdOld = persistentVuelo.getAeropuertoorigenId();
            Aeropuerto aeropuertoorigenIdNew = vuelo.getAeropuertoorigenId();
            Collection<Venta> ventaCollectionOld = persistentVuelo.getVentaCollection();
            Collection<Venta> ventaCollectionNew = vuelo.getVentaCollection();
            if (aerolineaIdNew != null) {
                aerolineaIdNew = em.getReference(aerolineaIdNew.getClass(), aerolineaIdNew.getId());
                vuelo.setAerolineaId(aerolineaIdNew);
            }
            if (aeropuertodestinoIdNew != null) {
                aeropuertodestinoIdNew = em.getReference(aeropuertodestinoIdNew.getClass(), aeropuertodestinoIdNew.getId());
                vuelo.setAeropuertodestinoId(aeropuertodestinoIdNew);
            }
            if (aeropuertoorigenIdNew != null) {
                aeropuertoorigenIdNew = em.getReference(aeropuertoorigenIdNew.getClass(), aeropuertoorigenIdNew.getId());
                vuelo.setAeropuertoorigenId(aeropuertoorigenIdNew);
            }
            Collection<Venta> attachedVentaCollectionNew = new ArrayList<Venta>();
            for (Venta ventaCollectionNewVentaToAttach : ventaCollectionNew) {
                ventaCollectionNewVentaToAttach = em.getReference(ventaCollectionNewVentaToAttach.getClass(), ventaCollectionNewVentaToAttach.getId());
                attachedVentaCollectionNew.add(ventaCollectionNewVentaToAttach);
            }
            ventaCollectionNew = attachedVentaCollectionNew;
            vuelo.setVentaCollection(ventaCollectionNew);
            vuelo = em.merge(vuelo);
            if (aerolineaIdOld != null && !aerolineaIdOld.equals(aerolineaIdNew)) {
                aerolineaIdOld.getVueloCollection().remove(vuelo);
                aerolineaIdOld = em.merge(aerolineaIdOld);
            }
            if (aerolineaIdNew != null && !aerolineaIdNew.equals(aerolineaIdOld)) {
                aerolineaIdNew.getVueloCollection().add(vuelo);
                aerolineaIdNew = em.merge(aerolineaIdNew);
            }
            if (aeropuertodestinoIdOld != null && !aeropuertodestinoIdOld.equals(aeropuertodestinoIdNew)) {
                aeropuertodestinoIdOld.getVueloCollection().remove(vuelo);
                aeropuertodestinoIdOld = em.merge(aeropuertodestinoIdOld);
            }
            if (aeropuertodestinoIdNew != null && !aeropuertodestinoIdNew.equals(aeropuertodestinoIdOld)) {
                aeropuertodestinoIdNew.getVueloCollection().add(vuelo);
                aeropuertodestinoIdNew = em.merge(aeropuertodestinoIdNew);
            }
            if (aeropuertoorigenIdOld != null && !aeropuertoorigenIdOld.equals(aeropuertoorigenIdNew)) {
                aeropuertoorigenIdOld.getVueloCollection().remove(vuelo);
                aeropuertoorigenIdOld = em.merge(aeropuertoorigenIdOld);
            }
            if (aeropuertoorigenIdNew != null && !aeropuertoorigenIdNew.equals(aeropuertoorigenIdOld)) {
                aeropuertoorigenIdNew.getVueloCollection().add(vuelo);
                aeropuertoorigenIdNew = em.merge(aeropuertoorigenIdNew);
            }
            for (Venta ventaCollectionOldVenta : ventaCollectionOld) {
                if (!ventaCollectionNew.contains(ventaCollectionOldVenta)) {
                    ventaCollectionOldVenta.setVueloId(null);
                    ventaCollectionOldVenta = em.merge(ventaCollectionOldVenta);
                }
            }
            for (Venta ventaCollectionNewVenta : ventaCollectionNew) {
                if (!ventaCollectionOld.contains(ventaCollectionNewVenta)) {
                    Vuelo oldVueloIdOfVentaCollectionNewVenta = ventaCollectionNewVenta.getVueloId();
                    ventaCollectionNewVenta.setVueloId(vuelo);
                    ventaCollectionNewVenta = em.merge(ventaCollectionNewVenta);
                    if (oldVueloIdOfVentaCollectionNewVenta != null && !oldVueloIdOfVentaCollectionNewVenta.equals(vuelo)) {
                        oldVueloIdOfVentaCollectionNewVenta.getVentaCollection().remove(ventaCollectionNewVenta);
                        oldVueloIdOfVentaCollectionNewVenta = em.merge(oldVueloIdOfVentaCollectionNewVenta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = vuelo.getId();
                if (findVuelo(id) == null) {
                    throw new NonexistentEntityException("The vuelo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vuelo vuelo;
            try {
                vuelo = em.getReference(Vuelo.class, id);
                vuelo.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vuelo with id " + id + " no longer exists.", enfe);
            }
            Aerolinea aerolineaId = vuelo.getAerolineaId();
            if (aerolineaId != null) {
                aerolineaId.getVueloCollection().remove(vuelo);
                aerolineaId = em.merge(aerolineaId);
            }
            Aeropuerto aeropuertodestinoId = vuelo.getAeropuertodestinoId();
            if (aeropuertodestinoId != null) {
                aeropuertodestinoId.getVueloCollection().remove(vuelo);
                aeropuertodestinoId = em.merge(aeropuertodestinoId);
            }
            Aeropuerto aeropuertoorigenId = vuelo.getAeropuertoorigenId();
            if (aeropuertoorigenId != null) {
                aeropuertoorigenId.getVueloCollection().remove(vuelo);
                aeropuertoorigenId = em.merge(aeropuertoorigenId);
            }
            Collection<Venta> ventaCollection = vuelo.getVentaCollection();
            for (Venta ventaCollectionVenta : ventaCollection) {
                ventaCollectionVenta.setVueloId(null);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
            }
            em.remove(vuelo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vuelo> findVueloEntities() {
        return findVueloEntities(true, -1, -1);
    }

    public List<Vuelo> findVueloEntities(int maxResults, int firstResult) {
        return findVueloEntities(false, maxResults, firstResult);
    }

    private List<Vuelo> findVueloEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vuelo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Vuelo findVuelo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vuelo.class, id);
        } finally {
            em.close();
        }
    }

    public int getVueloCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vuelo> rt = cq.from(Vuelo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
