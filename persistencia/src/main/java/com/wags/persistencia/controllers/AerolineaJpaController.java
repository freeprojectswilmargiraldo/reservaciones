/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.persistencia.controllers;

import com.wags.persistencia.controllers.exceptions.IllegalOrphanException;
import com.wags.persistencia.controllers.exceptions.NonexistentEntityException;
import com.wags.persistencia.entities.Aerolinea;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.wags.persistencia.entities.Vuelo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wilmar Giraldo
 */
public class AerolineaJpaController implements Serializable {

    public AerolineaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Aerolinea aerolinea) {
        if (aerolinea.getVueloCollection() == null) {
            aerolinea.setVueloCollection(new ArrayList<Vuelo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Vuelo> attachedVueloCollection = new ArrayList<Vuelo>();
            for (Vuelo vueloCollectionVueloToAttach : aerolinea.getVueloCollection()) {
                vueloCollectionVueloToAttach = em.getReference(vueloCollectionVueloToAttach.getClass(), vueloCollectionVueloToAttach.getId());
                attachedVueloCollection.add(vueloCollectionVueloToAttach);
            }
            aerolinea.setVueloCollection(attachedVueloCollection);
            em.persist(aerolinea);
            for (Vuelo vueloCollectionVuelo : aerolinea.getVueloCollection()) {
                Aerolinea oldAerolineaIdOfVueloCollectionVuelo = vueloCollectionVuelo.getAerolineaId();
                vueloCollectionVuelo.setAerolineaId(aerolinea);
                vueloCollectionVuelo = em.merge(vueloCollectionVuelo);
                if (oldAerolineaIdOfVueloCollectionVuelo != null) {
                    oldAerolineaIdOfVueloCollectionVuelo.getVueloCollection().remove(vueloCollectionVuelo);
                    oldAerolineaIdOfVueloCollectionVuelo = em.merge(oldAerolineaIdOfVueloCollectionVuelo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Aerolinea aerolinea) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Aerolinea persistentAerolinea = em.find(Aerolinea.class, aerolinea.getId());
            Collection<Vuelo> vueloCollectionOld = persistentAerolinea.getVueloCollection();
            Collection<Vuelo> vueloCollectionNew = aerolinea.getVueloCollection();
            List<String> illegalOrphanMessages = null;
            for (Vuelo vueloCollectionOldVuelo : vueloCollectionOld) {
                if (!vueloCollectionNew.contains(vueloCollectionOldVuelo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vuelo " + vueloCollectionOldVuelo + " since its aerolineaId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Vuelo> attachedVueloCollectionNew = new ArrayList<Vuelo>();
            for (Vuelo vueloCollectionNewVueloToAttach : vueloCollectionNew) {
                vueloCollectionNewVueloToAttach = em.getReference(vueloCollectionNewVueloToAttach.getClass(), vueloCollectionNewVueloToAttach.getId());
                attachedVueloCollectionNew.add(vueloCollectionNewVueloToAttach);
            }
            vueloCollectionNew = attachedVueloCollectionNew;
            aerolinea.setVueloCollection(vueloCollectionNew);
            aerolinea = em.merge(aerolinea);
            for (Vuelo vueloCollectionNewVuelo : vueloCollectionNew) {
                if (!vueloCollectionOld.contains(vueloCollectionNewVuelo)) {
                    Aerolinea oldAerolineaIdOfVueloCollectionNewVuelo = vueloCollectionNewVuelo.getAerolineaId();
                    vueloCollectionNewVuelo.setAerolineaId(aerolinea);
                    vueloCollectionNewVuelo = em.merge(vueloCollectionNewVuelo);
                    if (oldAerolineaIdOfVueloCollectionNewVuelo != null && !oldAerolineaIdOfVueloCollectionNewVuelo.equals(aerolinea)) {
                        oldAerolineaIdOfVueloCollectionNewVuelo.getVueloCollection().remove(vueloCollectionNewVuelo);
                        oldAerolineaIdOfVueloCollectionNewVuelo = em.merge(oldAerolineaIdOfVueloCollectionNewVuelo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = aerolinea.getId();
                if (findAerolinea(id) == null) {
                    throw new NonexistentEntityException("The aerolinea with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Aerolinea aerolinea;
            try {
                aerolinea = em.getReference(Aerolinea.class, id);
                aerolinea.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The aerolinea with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Vuelo> vueloCollectionOrphanCheck = aerolinea.getVueloCollection();
            for (Vuelo vueloCollectionOrphanCheckVuelo : vueloCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Aerolinea (" + aerolinea + ") cannot be destroyed since the Vuelo " + vueloCollectionOrphanCheckVuelo + " in its vueloCollection field has a non-nullable aerolineaId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(aerolinea);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Aerolinea> findAerolineaEntities() {
        return findAerolineaEntities(true, -1, -1);
    }

    public List<Aerolinea> findAerolineaEntities(int maxResults, int firstResult) {
        return findAerolineaEntities(false, maxResults, firstResult);
    }

    private List<Aerolinea> findAerolineaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Aerolinea.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Aerolinea findAerolinea(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Aerolinea.class, id);
        } finally {
            em.close();
        }
    }

    public int getAerolineaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Aerolinea> rt = cq.from(Aerolinea.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
