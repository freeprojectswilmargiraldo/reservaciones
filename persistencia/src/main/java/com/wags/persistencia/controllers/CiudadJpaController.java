/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.persistencia.controllers;

import com.wags.persistencia.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.wags.persistencia.entities.Departamento;
import com.wags.persistencia.entities.Aeropuerto;
import com.wags.persistencia.entities.Ciudad;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wilmar Giraldo
 */
public class CiudadJpaController implements Serializable {

    public CiudadJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ciudad ciudad) {
        if (ciudad.getAeropuertoCollection() == null) {
            ciudad.setAeropuertoCollection(new ArrayList<Aeropuerto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamento departamentoId = ciudad.getDepartamentoId();
            if (departamentoId != null) {
                departamentoId = em.getReference(departamentoId.getClass(), departamentoId.getId());
                ciudad.setDepartamentoId(departamentoId);
            }
            Collection<Aeropuerto> attachedAeropuertoCollection = new ArrayList<Aeropuerto>();
            for (Aeropuerto aeropuertoCollectionAeropuertoToAttach : ciudad.getAeropuertoCollection()) {
                aeropuertoCollectionAeropuertoToAttach = em.getReference(aeropuertoCollectionAeropuertoToAttach.getClass(), aeropuertoCollectionAeropuertoToAttach.getId());
                attachedAeropuertoCollection.add(aeropuertoCollectionAeropuertoToAttach);
            }
            ciudad.setAeropuertoCollection(attachedAeropuertoCollection);
            em.persist(ciudad);
            if (departamentoId != null) {
                departamentoId.getCiudadCollection().add(ciudad);
                departamentoId = em.merge(departamentoId);
            }
            for (Aeropuerto aeropuertoCollectionAeropuerto : ciudad.getAeropuertoCollection()) {
                Ciudad oldCiudadIdOfAeropuertoCollectionAeropuerto = aeropuertoCollectionAeropuerto.getCiudadId();
                aeropuertoCollectionAeropuerto.setCiudadId(ciudad);
                aeropuertoCollectionAeropuerto = em.merge(aeropuertoCollectionAeropuerto);
                if (oldCiudadIdOfAeropuertoCollectionAeropuerto != null) {
                    oldCiudadIdOfAeropuertoCollectionAeropuerto.getAeropuertoCollection().remove(aeropuertoCollectionAeropuerto);
                    oldCiudadIdOfAeropuertoCollectionAeropuerto = em.merge(oldCiudadIdOfAeropuertoCollectionAeropuerto);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ciudad ciudad) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ciudad persistentCiudad = em.find(Ciudad.class, ciudad.getId());
            Departamento departamentoIdOld = persistentCiudad.getDepartamentoId();
            Departamento departamentoIdNew = ciudad.getDepartamentoId();
            Collection<Aeropuerto> aeropuertoCollectionOld = persistentCiudad.getAeropuertoCollection();
            Collection<Aeropuerto> aeropuertoCollectionNew = ciudad.getAeropuertoCollection();
            if (departamentoIdNew != null) {
                departamentoIdNew = em.getReference(departamentoIdNew.getClass(), departamentoIdNew.getId());
                ciudad.setDepartamentoId(departamentoIdNew);
            }
            Collection<Aeropuerto> attachedAeropuertoCollectionNew = new ArrayList<Aeropuerto>();
            for (Aeropuerto aeropuertoCollectionNewAeropuertoToAttach : aeropuertoCollectionNew) {
                aeropuertoCollectionNewAeropuertoToAttach = em.getReference(aeropuertoCollectionNewAeropuertoToAttach.getClass(), aeropuertoCollectionNewAeropuertoToAttach.getId());
                attachedAeropuertoCollectionNew.add(aeropuertoCollectionNewAeropuertoToAttach);
            }
            aeropuertoCollectionNew = attachedAeropuertoCollectionNew;
            ciudad.setAeropuertoCollection(aeropuertoCollectionNew);
            ciudad = em.merge(ciudad);
            if (departamentoIdOld != null && !departamentoIdOld.equals(departamentoIdNew)) {
                departamentoIdOld.getCiudadCollection().remove(ciudad);
                departamentoIdOld = em.merge(departamentoIdOld);
            }
            if (departamentoIdNew != null && !departamentoIdNew.equals(departamentoIdOld)) {
                departamentoIdNew.getCiudadCollection().add(ciudad);
                departamentoIdNew = em.merge(departamentoIdNew);
            }
            for (Aeropuerto aeropuertoCollectionOldAeropuerto : aeropuertoCollectionOld) {
                if (!aeropuertoCollectionNew.contains(aeropuertoCollectionOldAeropuerto)) {
                    aeropuertoCollectionOldAeropuerto.setCiudadId(null);
                    aeropuertoCollectionOldAeropuerto = em.merge(aeropuertoCollectionOldAeropuerto);
                }
            }
            for (Aeropuerto aeropuertoCollectionNewAeropuerto : aeropuertoCollectionNew) {
                if (!aeropuertoCollectionOld.contains(aeropuertoCollectionNewAeropuerto)) {
                    Ciudad oldCiudadIdOfAeropuertoCollectionNewAeropuerto = aeropuertoCollectionNewAeropuerto.getCiudadId();
                    aeropuertoCollectionNewAeropuerto.setCiudadId(ciudad);
                    aeropuertoCollectionNewAeropuerto = em.merge(aeropuertoCollectionNewAeropuerto);
                    if (oldCiudadIdOfAeropuertoCollectionNewAeropuerto != null && !oldCiudadIdOfAeropuertoCollectionNewAeropuerto.equals(ciudad)) {
                        oldCiudadIdOfAeropuertoCollectionNewAeropuerto.getAeropuertoCollection().remove(aeropuertoCollectionNewAeropuerto);
                        oldCiudadIdOfAeropuertoCollectionNewAeropuerto = em.merge(oldCiudadIdOfAeropuertoCollectionNewAeropuerto);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ciudad.getId();
                if (findCiudad(id) == null) {
                    throw new NonexistentEntityException("The ciudad with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ciudad ciudad;
            try {
                ciudad = em.getReference(Ciudad.class, id);
                ciudad.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ciudad with id " + id + " no longer exists.", enfe);
            }
            Departamento departamentoId = ciudad.getDepartamentoId();
            if (departamentoId != null) {
                departamentoId.getCiudadCollection().remove(ciudad);
                departamentoId = em.merge(departamentoId);
            }
            Collection<Aeropuerto> aeropuertoCollection = ciudad.getAeropuertoCollection();
            for (Aeropuerto aeropuertoCollectionAeropuerto : aeropuertoCollection) {
                aeropuertoCollectionAeropuerto.setCiudadId(null);
                aeropuertoCollectionAeropuerto = em.merge(aeropuertoCollectionAeropuerto);
            }
            em.remove(ciudad);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ciudad> findCiudadEntities() {
        return findCiudadEntities(true, -1, -1);
    }

    public List<Ciudad> findCiudadEntities(int maxResults, int firstResult) {
        return findCiudadEntities(false, maxResults, firstResult);
    }

    private List<Ciudad> findCiudadEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ciudad.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ciudad findCiudad(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ciudad.class, id);
        } finally {
            em.close();
        }
    }

    public int getCiudadCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ciudad> rt = cq.from(Ciudad.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
