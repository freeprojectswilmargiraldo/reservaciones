/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.persistencia.controllers;

import com.wags.persistencia.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.wags.persistencia.entities.Cliente;
import com.wags.persistencia.entities.Venta;
import com.wags.persistencia.entities.Vuelo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wilmar Giraldo
 */
public class VentaJpaController implements Serializable {

    public VentaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Venta venta) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente clienteId = venta.getClienteId();
            if (clienteId != null) {
                clienteId = em.getReference(clienteId.getClass(), clienteId.getId());
                venta.setClienteId(clienteId);
            }
            Vuelo vueloId = venta.getVueloId();
            if (vueloId != null) {
                vueloId = em.getReference(vueloId.getClass(), vueloId.getId());
                venta.setVueloId(vueloId);
            }
            em.persist(venta);
            if (clienteId != null) {
                clienteId.getVentaCollection().add(venta);
                clienteId = em.merge(clienteId);
            }
            if (vueloId != null) {
                vueloId.getVentaCollection().add(venta);
                vueloId = em.merge(vueloId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Venta venta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta persistentVenta = em.find(Venta.class, venta.getId());
            Cliente clienteIdOld = persistentVenta.getClienteId();
            Cliente clienteIdNew = venta.getClienteId();
            Vuelo vueloIdOld = persistentVenta.getVueloId();
            Vuelo vueloIdNew = venta.getVueloId();
            if (clienteIdNew != null) {
                clienteIdNew = em.getReference(clienteIdNew.getClass(), clienteIdNew.getId());
                venta.setClienteId(clienteIdNew);
            }
            if (vueloIdNew != null) {
                vueloIdNew = em.getReference(vueloIdNew.getClass(), vueloIdNew.getId());
                venta.setVueloId(vueloIdNew);
            }
            venta = em.merge(venta);
            if (clienteIdOld != null && !clienteIdOld.equals(clienteIdNew)) {
                clienteIdOld.getVentaCollection().remove(venta);
                clienteIdOld = em.merge(clienteIdOld);
            }
            if (clienteIdNew != null && !clienteIdNew.equals(clienteIdOld)) {
                clienteIdNew.getVentaCollection().add(venta);
                clienteIdNew = em.merge(clienteIdNew);
            }
            if (vueloIdOld != null && !vueloIdOld.equals(vueloIdNew)) {
                vueloIdOld.getVentaCollection().remove(venta);
                vueloIdOld = em.merge(vueloIdOld);
            }
            if (vueloIdNew != null && !vueloIdNew.equals(vueloIdOld)) {
                vueloIdNew.getVentaCollection().add(venta);
                vueloIdNew = em.merge(vueloIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = venta.getId();
                if (findVenta(id) == null) {
                    throw new NonexistentEntityException("The venta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta venta;
            try {
                venta = em.getReference(Venta.class, id);
                venta.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The venta with id " + id + " no longer exists.", enfe);
            }
            Cliente clienteId = venta.getClienteId();
            if (clienteId != null) {
                clienteId.getVentaCollection().remove(venta);
                clienteId = em.merge(clienteId);
            }
            Vuelo vueloId = venta.getVueloId();
            if (vueloId != null) {
                vueloId.getVentaCollection().remove(venta);
                vueloId = em.merge(vueloId);
            }
            em.remove(venta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Venta> findVentaEntities() {
        return findVentaEntities(true, -1, -1);
    }

    public List<Venta> findVentaEntities(int maxResults, int firstResult) {
        return findVentaEntities(false, maxResults, firstResult);
    }

    private List<Venta> findVentaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Venta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Venta findVenta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Venta.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Venta> rt = cq.from(Venta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
