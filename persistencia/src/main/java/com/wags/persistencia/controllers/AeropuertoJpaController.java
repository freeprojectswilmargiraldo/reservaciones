/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.persistencia.controllers;

import com.wags.persistencia.controllers.exceptions.IllegalOrphanException;
import com.wags.persistencia.controllers.exceptions.NonexistentEntityException;
import com.wags.persistencia.entities.Aeropuerto;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.wags.persistencia.entities.Ciudad;
import com.wags.persistencia.entities.Vuelo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wilmar Giraldo
 */
public class AeropuertoJpaController implements Serializable {

    public AeropuertoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Aeropuerto aeropuerto) {
        if (aeropuerto.getVueloCollection() == null) {
            aeropuerto.setVueloCollection(new ArrayList<Vuelo>());
        }
        if (aeropuerto.getVueloCollection1() == null) {
            aeropuerto.setVueloCollection1(new ArrayList<Vuelo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ciudad ciudadId = aeropuerto.getCiudadId();
            if (ciudadId != null) {
                ciudadId = em.getReference(ciudadId.getClass(), ciudadId.getId());
                aeropuerto.setCiudadId(ciudadId);
            }
            Collection<Vuelo> attachedVueloCollection = new ArrayList<Vuelo>();
            for (Vuelo vueloCollectionVueloToAttach : aeropuerto.getVueloCollection()) {
                vueloCollectionVueloToAttach = em.getReference(vueloCollectionVueloToAttach.getClass(), vueloCollectionVueloToAttach.getId());
                attachedVueloCollection.add(vueloCollectionVueloToAttach);
            }
            aeropuerto.setVueloCollection(attachedVueloCollection);
            Collection<Vuelo> attachedVueloCollection1 = new ArrayList<Vuelo>();
            for (Vuelo vueloCollection1VueloToAttach : aeropuerto.getVueloCollection1()) {
                vueloCollection1VueloToAttach = em.getReference(vueloCollection1VueloToAttach.getClass(), vueloCollection1VueloToAttach.getId());
                attachedVueloCollection1.add(vueloCollection1VueloToAttach);
            }
            aeropuerto.setVueloCollection1(attachedVueloCollection1);
            em.persist(aeropuerto);
            if (ciudadId != null) {
                ciudadId.getAeropuertoCollection().add(aeropuerto);
                ciudadId = em.merge(ciudadId);
            }
            for (Vuelo vueloCollectionVuelo : aeropuerto.getVueloCollection()) {
                Aeropuerto oldAeropuertodestinoIdOfVueloCollectionVuelo = vueloCollectionVuelo.getAeropuertodestinoId();
                vueloCollectionVuelo.setAeropuertodestinoId(aeropuerto);
                vueloCollectionVuelo = em.merge(vueloCollectionVuelo);
                if (oldAeropuertodestinoIdOfVueloCollectionVuelo != null) {
                    oldAeropuertodestinoIdOfVueloCollectionVuelo.getVueloCollection().remove(vueloCollectionVuelo);
                    oldAeropuertodestinoIdOfVueloCollectionVuelo = em.merge(oldAeropuertodestinoIdOfVueloCollectionVuelo);
                }
            }
            for (Vuelo vueloCollection1Vuelo : aeropuerto.getVueloCollection1()) {
                Aeropuerto oldAeropuertoorigenIdOfVueloCollection1Vuelo = vueloCollection1Vuelo.getAeropuertoorigenId();
                vueloCollection1Vuelo.setAeropuertoorigenId(aeropuerto);
                vueloCollection1Vuelo = em.merge(vueloCollection1Vuelo);
                if (oldAeropuertoorigenIdOfVueloCollection1Vuelo != null) {
                    oldAeropuertoorigenIdOfVueloCollection1Vuelo.getVueloCollection1().remove(vueloCollection1Vuelo);
                    oldAeropuertoorigenIdOfVueloCollection1Vuelo = em.merge(oldAeropuertoorigenIdOfVueloCollection1Vuelo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Aeropuerto aeropuerto) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Aeropuerto persistentAeropuerto = em.find(Aeropuerto.class, aeropuerto.getId());
            Ciudad ciudadIdOld = persistentAeropuerto.getCiudadId();
            Ciudad ciudadIdNew = aeropuerto.getCiudadId();
            Collection<Vuelo> vueloCollectionOld = persistentAeropuerto.getVueloCollection();
            Collection<Vuelo> vueloCollectionNew = aeropuerto.getVueloCollection();
            Collection<Vuelo> vueloCollection1Old = persistentAeropuerto.getVueloCollection1();
            Collection<Vuelo> vueloCollection1New = aeropuerto.getVueloCollection1();
            List<String> illegalOrphanMessages = null;
            for (Vuelo vueloCollectionOldVuelo : vueloCollectionOld) {
                if (!vueloCollectionNew.contains(vueloCollectionOldVuelo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vuelo " + vueloCollectionOldVuelo + " since its aeropuertodestinoId field is not nullable.");
                }
            }
            for (Vuelo vueloCollection1OldVuelo : vueloCollection1Old) {
                if (!vueloCollection1New.contains(vueloCollection1OldVuelo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vuelo " + vueloCollection1OldVuelo + " since its aeropuertoorigenId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (ciudadIdNew != null) {
                ciudadIdNew = em.getReference(ciudadIdNew.getClass(), ciudadIdNew.getId());
                aeropuerto.setCiudadId(ciudadIdNew);
            }
            Collection<Vuelo> attachedVueloCollectionNew = new ArrayList<Vuelo>();
            for (Vuelo vueloCollectionNewVueloToAttach : vueloCollectionNew) {
                vueloCollectionNewVueloToAttach = em.getReference(vueloCollectionNewVueloToAttach.getClass(), vueloCollectionNewVueloToAttach.getId());
                attachedVueloCollectionNew.add(vueloCollectionNewVueloToAttach);
            }
            vueloCollectionNew = attachedVueloCollectionNew;
            aeropuerto.setVueloCollection(vueloCollectionNew);
            Collection<Vuelo> attachedVueloCollection1New = new ArrayList<Vuelo>();
            for (Vuelo vueloCollection1NewVueloToAttach : vueloCollection1New) {
                vueloCollection1NewVueloToAttach = em.getReference(vueloCollection1NewVueloToAttach.getClass(), vueloCollection1NewVueloToAttach.getId());
                attachedVueloCollection1New.add(vueloCollection1NewVueloToAttach);
            }
            vueloCollection1New = attachedVueloCollection1New;
            aeropuerto.setVueloCollection1(vueloCollection1New);
            aeropuerto = em.merge(aeropuerto);
            if (ciudadIdOld != null && !ciudadIdOld.equals(ciudadIdNew)) {
                ciudadIdOld.getAeropuertoCollection().remove(aeropuerto);
                ciudadIdOld = em.merge(ciudadIdOld);
            }
            if (ciudadIdNew != null && !ciudadIdNew.equals(ciudadIdOld)) {
                ciudadIdNew.getAeropuertoCollection().add(aeropuerto);
                ciudadIdNew = em.merge(ciudadIdNew);
            }
            for (Vuelo vueloCollectionNewVuelo : vueloCollectionNew) {
                if (!vueloCollectionOld.contains(vueloCollectionNewVuelo)) {
                    Aeropuerto oldAeropuertodestinoIdOfVueloCollectionNewVuelo = vueloCollectionNewVuelo.getAeropuertodestinoId();
                    vueloCollectionNewVuelo.setAeropuertodestinoId(aeropuerto);
                    vueloCollectionNewVuelo = em.merge(vueloCollectionNewVuelo);
                    if (oldAeropuertodestinoIdOfVueloCollectionNewVuelo != null && !oldAeropuertodestinoIdOfVueloCollectionNewVuelo.equals(aeropuerto)) {
                        oldAeropuertodestinoIdOfVueloCollectionNewVuelo.getVueloCollection().remove(vueloCollectionNewVuelo);
                        oldAeropuertodestinoIdOfVueloCollectionNewVuelo = em.merge(oldAeropuertodestinoIdOfVueloCollectionNewVuelo);
                    }
                }
            }
            for (Vuelo vueloCollection1NewVuelo : vueloCollection1New) {
                if (!vueloCollection1Old.contains(vueloCollection1NewVuelo)) {
                    Aeropuerto oldAeropuertoorigenIdOfVueloCollection1NewVuelo = vueloCollection1NewVuelo.getAeropuertoorigenId();
                    vueloCollection1NewVuelo.setAeropuertoorigenId(aeropuerto);
                    vueloCollection1NewVuelo = em.merge(vueloCollection1NewVuelo);
                    if (oldAeropuertoorigenIdOfVueloCollection1NewVuelo != null && !oldAeropuertoorigenIdOfVueloCollection1NewVuelo.equals(aeropuerto)) {
                        oldAeropuertoorigenIdOfVueloCollection1NewVuelo.getVueloCollection1().remove(vueloCollection1NewVuelo);
                        oldAeropuertoorigenIdOfVueloCollection1NewVuelo = em.merge(oldAeropuertoorigenIdOfVueloCollection1NewVuelo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = aeropuerto.getId();
                if (findAeropuerto(id) == null) {
                    throw new NonexistentEntityException("The aeropuerto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Aeropuerto aeropuerto;
            try {
                aeropuerto = em.getReference(Aeropuerto.class, id);
                aeropuerto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The aeropuerto with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Vuelo> vueloCollectionOrphanCheck = aeropuerto.getVueloCollection();
            for (Vuelo vueloCollectionOrphanCheckVuelo : vueloCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Aeropuerto (" + aeropuerto + ") cannot be destroyed since the Vuelo " + vueloCollectionOrphanCheckVuelo + " in its vueloCollection field has a non-nullable aeropuertodestinoId field.");
            }
            Collection<Vuelo> vueloCollection1OrphanCheck = aeropuerto.getVueloCollection1();
            for (Vuelo vueloCollection1OrphanCheckVuelo : vueloCollection1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Aeropuerto (" + aeropuerto + ") cannot be destroyed since the Vuelo " + vueloCollection1OrphanCheckVuelo + " in its vueloCollection1 field has a non-nullable aeropuertoorigenId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Ciudad ciudadId = aeropuerto.getCiudadId();
            if (ciudadId != null) {
                ciudadId.getAeropuertoCollection().remove(aeropuerto);
                ciudadId = em.merge(ciudadId);
            }
            em.remove(aeropuerto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Aeropuerto> findAeropuertoEntities() {
        return findAeropuertoEntities(true, -1, -1);
    }

    public List<Aeropuerto> findAeropuertoEntities(int maxResults, int firstResult) {
        return findAeropuertoEntities(false, maxResults, firstResult);
    }

    private List<Aeropuerto> findAeropuertoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Aeropuerto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Aeropuerto findAeropuerto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Aeropuerto.class, id);
        } finally {
            em.close();
        }
    }

    public int getAeropuertoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Aeropuerto> rt = cq.from(Aeropuerto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
