package com.wags.persistencia;

import com.wags.persistencia.controllers.ClienteJpaController;
import com.wags.persistencia.entities.Cliente;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Wilmar Giraldo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        String fechaNacimiento;

        // TODO: Crear la información para poblar la base de datos con info de
        // ejemplo
        try {

            ClienteJpaController dao_cliente = new ClienteJpaController(emf);
            Cliente nuevoCliente;
            nuevoCliente = new Cliente();
            nuevoCliente.setNombre("Wilmar Giraldo");
            nuevoCliente.setEmail("wilmar.giraldo@gmail.com");
            nuevoCliente.setCedula("80721328");
            fechaNacimiento = "4/10/1981";
            nuevoCliente.setFechanacimiento(sdf.parse(fechaNacimiento));
            dao_cliente.create(nuevoCliente);

            nuevoCliente = null;
            nuevoCliente = new Cliente();
            nuevoCliente.setNombre("Andrea Martinez");
            nuevoCliente.setEmail("andrem.8923@gmail.com");
            nuevoCliente.setCedula("1128472520");
            fechaNacimiento = "23/10/1992";
            nuevoCliente.setFechanacimiento(sdf.parse(fechaNacimiento));
            dao_cliente.create(nuevoCliente);

            nuevoCliente = null;
            nuevoCliente = new Cliente();
            nuevoCliente.setNombre("Leisly Giraldo");
            nuevoCliente.setEmail("legirald@misena.edu.co");
            nuevoCliente.setCedula("1128440370");
            fechaNacimiento = "4/3/1991";
            nuevoCliente.setFechanacimiento(sdf.parse(fechaNacimiento));
            dao_cliente.create(nuevoCliente);
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Error al intentar obtener una fecha valida", ex);
        }
        
        // Aerolineas

    }

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU_vuelos");
}
